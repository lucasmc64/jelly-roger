![Jelly Roger](./readme/cover.png)

# 🏴‍☠️ Jelly Roger

> Note: Make no mistake, there is no encouragement or support for piracy here.

## :dart: Goal

We are full of dreams. But while we don't catch up with One Piece and [Jellyfin](https://jellyfin.org/) redesigns its interface, this project aims to apply modern design concepts by creating custom styles for the platform.

## :scroll: Some details

This project was created using tools to make development as easy as possible:

- [Node.JS](https://www.npmjs.com/package/@pnp/sp)
  It is only necessary to globally install the following packages that facilitate development.
- [Sass](https://www.npmjs.com/package/sass)
  CSS preprocessor that, in addition to adding extra features, encourages an architecture to only organize style files.
- [Live Server](https://www.npmjs.com/package/live-server) or [Python](https://www.python.org/)
  Serve the project locally to test and preview changes during development.

## 🪬 Overview

### 🧑‍💻 Developers

- **Lucas Coutinho**
  - GitHub: https://github.com/lucasmc64
  - LinkedIn: https://www.linkedin.com/in/lucasmc64/

### 🔗 Useful links

**Board**: https://tree.taiga.io/project/lucasmarcal-jellyroger/kanban

**Repository**: https://gitlab.com/alfred-lab/jelly-roger

**CDN**: Hosted in ?

### 🐙 Repository details

Below is the branch structure/pattern created for this project:

- **main**: If there is more than one developer or the sprint tasks are divided, this will be the place to gather the codes and resolve conflicts before the modifications go to testing.
- **TG-(number)**: It covers all the code produced to meet the needs exposed by a task and uses the same code as the task created in Taiga.

## :thinking: How do I run the project on my machine?

The first step is to clone the project, either via terminal or even by downloading the compressed file (.zip). After that, go ahead.

### :hammer_and_wrench: Requirements

- [NodeJS and NPM](https://nodejs.org)
- (Optional) [Python](https://www.python.org/)
- (Optional) [Yarn](https://yarnpkg.com/)

### :mag: Installing dependencies

With Node.JS installed, you will need to install the `sass` package globally which is responsible for processing the .scss files and generating the .css files.

```bash
# Using NPM
npm i -g sass

# Using Yarn
yarn global add sass
```

Now, if you intend to use `live-server` to serve CSS, you will also need to install it globally.

> If you prefer to use Python for this, you don't need to install anything else.

```bash
# Using NPM
npm i -g live-server

# Using Yarn
yarn global add live-server
```

### :sparkles: Running the project

### In Linux magical world

If your operating system uses the Linux kernel, things are a little easier for you.

Access the project's root directory via terminal and grant permission for the `development.sh` script to be executed.

```bash
chmod +x ./development.sh
```

Now, still in the root directory, run the script informing through a flag which tool you prefer will be used to create the server ("live-server" or "python3", without the quotes).

```bash
# Using live-server
./development.sh --server=live-server

# Using python3
./development.sh --server=python3
```

### In other SOs

Possibly the script can be adapted to also run on MacOS, suggestions and contributions are welcome.

This script simply condenses the server's parallel execution with the processing of the .scss files. You can simply open two tabs in the terminal emulator and run the commands separately.

```bash
# Processing .scss files
sass ./src/sass/main.scss ./dist/jelly-roger.css --watch"
```

```bash
# Starting a server with live-server
live-server --no-browser --port=5640 ./dist

# Starting a server with python3
python3 -m http.server -d ./dist 5640
```

### :tada: If everything went well...

Now you are running the project beautifully!

### 😶 ...and now?

If you did not change the server's default port, the CSS files should be exported at http://localhost:5640.

So, with Jellyfin having access to your local network, go to Side Menu > Dashboard > General. At the bottom of the settings page there is a field called "Custom CSS code" where you can import styles.

> If your Jellyfin is hosted in the cloud or another network, you can use tools like [Tailscale](https://tailscale.com) to make it possible for the machine you're developing on and the device where Jellyfin is running to interact with each other.

```css
/* Example: Importing the same file in different ways */

/* Using local IP address */
@import url("http://192.168.1.10:5640/jelly-roger.css");

/* Using Tailscale magic addresses */
@import url("http://zorojuro:5640/jelly-roger.css");
@import url("http://zorojuro.merry-go.ts.net:5640/jelly-roger.css");
@import url("http://100.73.160.123:5640/jelly-roger.css");
```

![Jellyfin: Custom CSS code field](./readme//jellyfin-css-import.png)

Note that the address used in `import` must be changed according to your local address or those provided by Tailscale to you.

---

Made with 💜 by a dreamer

#!/bin/bash

## Constants

SUPPORTED_SERVERS=(python3 live-server)
readonly SUPPORTED_SERVERS

RED_COLOR="\e[91m"
readonly RED_COLOR

RESET_COLOR="\e[0m"
readonly RESET_COLOR

readonly PORT=5640
readonly PORT

SCRIPT_DIRECTORY=$(pwd -P)
readonly SCRIPT_DIRECTORY

RUN_SASS_COMMAND="sass $SCRIPT_DIRECTORY/src/sass/main.scss $SCRIPT_DIRECTORY/dist/jelly-roger.css --watch"
readonly RUN_SASS_COMMAND

# Function responsible for show error message and stop script execution

error() {
  # $1 (first argument): Error message to be displayed before stop the script

  [[ ! -v $1 ]] && echo -ne "\n${RED_COLOR}✘${RESET_COLOR} $1\n"
  echo -e "\nExiting...\n"
  exit 1
}

verify_server_dependencies() {
  if
    ! sh -c "$1 --version" &>/dev/null
  then
    error "$1 is not installed correctly or this script does not have access to it."
  fi
}

# Getting server type

for arg in "$@"; do
  if [[ $arg == --server=* ]]; then
    IFS=" "
    SERVER="${arg:9}"

    if [[ ! "${IFS}${SUPPORTED_SERVERS[*]}${IFS}" =~ ${IFS}${SERVER}${IFS} ]]; then
      error "The server \"$SERVER\" is invalid or not supported."
    fi
  fi
done

if [ -z "$SERVER" ]; then
  error "The server was not informed."
fi

verify_server_dependencies "$SERVER"

case $SERVER in
"live-server")
  RUN_SERVER_CMD="live-server --no-browser --port=${PORT} ${SCRIPT_DIRECTORY}/dist"
  ;;

"python3")
  RUN_SERVER_CMD="python3 -m http.server -d ${SCRIPT_DIRECTORY}/dist ${PORT}"
  ;;
esac

COMMANDS=(
  "$RUN_SASS_COMMAND"
  "$RUN_SERVER_CMD"
)

printf "%s\n" "${COMMANDS[@]}" | xargs -I {} -P 2 sh -c "{}"
